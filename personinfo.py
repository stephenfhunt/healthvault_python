def nodeText(node):
    text = ''
    for n in node.childNodes:
        if n.nodeType == n.TEXT_NODE:
            text += n.data
    return text

class PersonInfo:
    
    def __init__(self, xmldata):
        import xml.dom.minidom  
        dom = xml.dom.minidom.parseString(xmldata)
        self.personId = nodeText(dom.getElementsByTagName('person-id')[0])
        self.recordId = nodeText(dom.getElementsByTagName('selected-record-id')[0])
        self.name = nodeText(dom.getElementsByTagName('name')[0])

