# Utilities and definitions of healthvault server-specific properties

# The relevant healthvault URLs
auth_shell_url='https://account.healthvault-ppe.com/redirect.aspx?target=AUTH&'
healthvault_server='platform.healthvault-ppe.com'
healthvault_path='/platform/wildcat.ashx'

def fillAuthUrl(url, applicationKey):
    '''Populate the Healthvault auth shell URL with our client's parameters

    url - our client's landing URL to be loaded after authentication
    applicationKey - our client's key to be verified by the server
    '''
    import urllib
    targetqs = [('appid', applicationKey)]
    targetqs.append( ('redirect', url) )
    return (auth_shell_url + urllib.urlencode([('targetqs', urllib.urlencode(targetqs))]))

