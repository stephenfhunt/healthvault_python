import datetime

def getNodeText(node):
    '''Retrieve text content for a node'''
    text = ''
    for n in node.childNodes:
        if n.nodeType == n.TEXT_NODE:
            text += n.data
    return text

def getNodeByPath(node, path):
    for p in path:
        node = node.getElementsByTagName(p)[0]
    return node

def getNodeTextByPath(node, path):
    return getNodeText(getNodeByPath(node, path))

class Allergy:
    typeid='52bf9104-2c5e-4f1f-a66d-552ebcc53df7'
    def __init__(self, node):
        self.typeName = node.getElementsByTagName('type-id')[0].getAttribute('name')
        self.name = getNodeText(getNodeByPath(node, ['name', 'text']))
        self.reaction = getNodeText(getNodeByPath(node, ['reaction', 'text']))

class Condition:
    typeid='7ea7a1f9-880b-4bd4-b593-f5660f20eda8'
    def __init__(self, node):
        self.typeName = node.getElementsByTagName('type-id')[0].getAttribute('name')
        self.name = getNodeText(getNodeByPath(node, ['name', 'text']))
        self.status = getNodeText(getNodeByPath(node, ['status', 'text']))

class PersonalDemo:
    typeid='92ba621e-66b3-4a01-bd73-74844aed4f5b'
    def __init__ (self, node):
        self.typeName = node.getElementsByTagName('type-id')[0].getAttribute('name')
        self.blood_type = getNodeText(getNodeByPath(node, ['blood-type', 'text']))
        year = getNodeTextByPath(node, ['birthdate', 'date', 'y'])
        month = getNodeTextByPath(node, ['birthdate', 'date', 'm'])
        day = getNodeTextByPath(node, ['birthdate', 'date', 'd'])
        self.birthdate = datetime.date(int(year), int(month), int(day))

class Medication:
    typeid='5C5F1223-F63C-4464-870C-3E36BA471DEF'
    def __init__ (self, node):
        self.typeName = node.getElementsByTagName('type-id')[0].getAttribute('name')
        self.name = getNodeText(getNodeByPath(node, ['name', 'text']))
        self.strength = getNodeText(getNodeByPath(node, ['strength', 'display']))
        self.indication = getNodeText(getNodeByPath(node, ['indication', 'text']))

class MedicalDevice:
    typeid='EF9CF8D5-6C0B-4292-997F-4047240BC7BE'
    def __init__ (self, node):
        self.typeName = node.getElementsByTagName('type-id')[0].getAttribute('name')
        self.device_name = getNodeText(getNodeByPath(node, ['device-name']))
        self.model = getNodeText(getNodeByPath(node, ['model']))
        self.anatomic_site = getNodeText(getNodeByPath(node, ['anatomic-site']))

class Contact:
    typeid='25c94a9f-9d3d-4576-96dc-6791178a8143'
    def __init__ (self, node):
        self.typeName = node.getElementsByTagName('type-id')[0].getAttribute('name')
        self.name = getNodeText(getNodeByPath(node, ['name', 'full']))
        self.type = getNodeText(getNodeByPath(node, ['type']))
        self.street = getNodeText(getNodeByPath(node, ['address', 'street']))
        self.city = getNodeText(getNodeByPath(node, ['address', 'city']))
        self.state = getNodeText(getNodeByPath(node, ['address', 'state']))
        self.postcode = getNodeText(getNodeByPath(node, ['address', 'postcode']))
        self.phone = getNodeText(getNodeByPath(node, ['phone', 'number']))
        self.email = getNodeText(getNodeByPath(node, ['email', 'address']))

class BloodGlucoseMeasurement:
    typeid='879e7c04-4e8a-4707-9ad3-b054df467ce4'
    def __init__(self, node):
        self.typeName = node.getElementsByTagName('type-id')[0].getAttribute('name')
        year = getNodeTextByPath(node, ['when', 'date', 'y'])
        month = getNodeTextByPath(node, ['when', 'date', 'm'])
        day = getNodeTextByPath(node, ['when', 'date', 'd'])
        self.date = datetime.date(int(year), int(month), int(day))
        self.value = getNodeTextByPath(node, ['value', 'display'])
        self.normalcy = getNodeTextByPath(node, ['normalcy'])

class BloodPressureMeasurement:
    typeid='ca3c57f4-f4c1-4e15-be67-0a3caf5414ed'
    def __init__(self, node):
        self.typeName = node.getElementsByTagName('type-id')[0].getAttribute('name')
        year = getNodeTextByPath(node, ['when', 'date', 'y'])
        month = getNodeTextByPath(node, ['when', 'date', 'm'])
        day = getNodeTextByPath(node, ['when', 'date', 'd'])
        self.date = datetime.date(int(year), int(month), int(day))
        self.systolic = getNodeTextByPath(node, ['systolic'])
        self.diastolic = getNodeTextByPath(node, ['diastolic'])
        self.pulse = getNodeTextByPath(node, ['pulse'])
        self.value = self.systolic + '/' + self.diastolic
        self.irregularHeartbeat = (getNodeTextByPath(node, ['pulse']) == 'true')

class HeightMeasurement:
    typeid='40750a6a-89b2-455c-bd8d-b420a4cb500b'
    def __init__(self, node):
        self.typeName = node.getElementsByTagName('type-id')[0].getAttribute('name')
        year = getNodeTextByPath(node, ['when', 'date', 'y'])
        month = getNodeTextByPath(node, ['when', 'date', 'm'])
        day = getNodeTextByPath(node, ['when', 'date', 'd'])
        self.date = datetime.date(int(year), int(month), int(day))
        self.value = getNodeTextByPath(node, ['value', 'm']) + 'm'

class WeightMeasurement:
    typeid='3d34d87e-7fc1-4153-800f-f56592cb0d17'
    def __init__(self, node):
        self.typeName = node.getElementsByTagName('type-id')[0].getAttribute('name')
        year = getNodeTextByPath(node, ['when', 'date', 'y'])
        month = getNodeTextByPath(node, ['when', 'date', 'm'])
        day = getNodeTextByPath(node, ['when', 'date', 'd'])
        self.date = datetime.date(int(year), int(month), int(day))
        self.value = getNodeTextByPath(node, ['value', 'kg']) + 'kg'

class Immunization:
    typeid='cd3587b5-b6e1-4565-ab3b-1c3ad45eb04f'
    def __init__(self, node):
        self.typeName = node.getElementsByTagName('type-id')[0].getAttribute('name')
        self.name = getNodeTextByPath(node, ['name', 'text'])
        year = getNodeTextByPath(node, ['administration-date', 'structured', 'date', 'y'])
        month = getNodeTextByPath(node, ['administration-date', 'structured', 'date', 'm'])
        day = getNodeTextByPath(node, ['administration-date', 'structured', 'date', 'd'])
        self.date = datetime.date(int(year), int(month), int(day))

class Procedure:
    typeid='0A5F9A43-DC88-4E9F-890F-1F9159B76E7B'
    def __init__(self, node):
        self.typeName = node.getElementsByTagName('type-id')[0].getAttribute('name')
        self.name = getNodeTextByPath(node, ['name', 'text'])
        year = getNodeTextByPath(node, ['when', 'date', 'y'])
        month = getNodeTextByPath(node, ['when', 'date', 'm'])
        day = getNodeTextByPath(node, ['when', 'date', 'd'])
        self.date = datetime.date(int(year), int(month), int(day))

class FamilyHistory:
    typeid='6D39F894-F7AC-4FCE-AC78-B22693BF96E6'
    def __init__(self, node):
        self.typeName = node.getElementsByTagName('type-id')[0].getAttribute('name')
        self.condition = getNodeTextByPath(node, ['condition', 'text'])
        #self.status = getNodeTextByPath(node, ['status', 'text'])
        self.relationship = getNodeTextByPath(node, ['relationship', 'text'])
        #self.ageOfOnset = getNodeTextByPath(node, ['age-of-onset'])

class File:
    typeid='bd0403c5-4ae2-4b0e-a8db-1888678e4528'
    def __init__(self, node):
        import base64
        self.typeName = node.getElementsByTagName('type-id')[0].getAttribute('name')
        self.filename = getNodeText(getNodeByPath(node, ['file', 'name']))
        self.contentType = getNodeText(getNodeByPath(node, ['content-type', 'text']))
        self.note = getNodeText(getNodeByPath(node, ['common', 'note']))
        data = getNodeText(getNodeByPath(node, ['data-other']))
        self.data = base64.b64decode(data)

