import things
import inspect

# Build a lookup table that we can use to construct an appropiate Thing class from
# a DOM node given to us by Healthvault
thingLookup = {}
for thingClass in inspect.getmembers(things, lambda n: return hasattr(n, 'typeid')):
    thingLookup[getattr(thingClass, 'typeid')] = thingClass

def create(node):
    name = node.getElementsByTagName('thing-id')[0].getAttribute('version-stamp')
    if name in thingLookup: return thingLookup[name](node)
    
